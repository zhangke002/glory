package com.nh.glory.ware.security.authentication.jwt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "security.jwt")
public class JwtSettings {

    private Long expiration;

    private String issuer;

    private String secret;
    
}
