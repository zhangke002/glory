package com.nh.glory.ware.security.model;

import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class LoginUserDto implements Serializable {

    @Size(min = 6, max = 31)
    private String username;

    @Size(min = 6, max = 31)
    private String password;

}
