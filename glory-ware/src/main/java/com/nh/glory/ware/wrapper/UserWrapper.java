/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nh.glory.ware.wrapper;

import com.nh.glory.core.util.BeanUtil;
import com.nh.glory.core.util.Func;
import com.nh.glory.data.model.User;
import com.nh.glory.data.vo.UserVO;
import com.nh.glory.ware.service.system.DictService;
import com.nh.glory.ware.service.system.UserService;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 包装类,返回视图层所需的字段
 *
 * @author Chill
 */
@AllArgsConstructor
public class UserWrapper extends BaseEntityWrapper<User, UserVO> {

	private UserService userService;

	private DictService dictService;

	public UserWrapper() {
	}

	@Override
	public UserVO entityVO(User user) {
		UserVO userVO = BeanUtil.copy(user, UserVO.class);
		List<String> roleName = userService.getRoleName(user.getRoleId().toString());
		List<String> deptName = userService.getDeptName(user.getDeptId().toString());
		userVO.setRoleName(Func.join(roleName));
		userVO.setDeptName(Func.join(deptName));
		String dict = dictService.getValue("sex", Func.toInt(user.getSex()));
		if (Func.isNotEmpty(dict)) {
			userVO.setSexName(dict);
		}

		String statusName = dictService.getValue("user_status", Func.toInt(user.getStatus()));
		if(Func.isNotEmpty(statusName)) {
			userVO.setStatusName(statusName);
		}
		return userVO;
	}

}
