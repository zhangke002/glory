package com.nh.glory.ware.service.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nh.glory.data.mapper.RoleMenuMapper;
import com.nh.glory.data.model.RoleMenu;
import org.springframework.stereotype.Service;

@Service
public class RoleMenuService extends ServiceImpl<RoleMenuMapper, RoleMenu> {
}
