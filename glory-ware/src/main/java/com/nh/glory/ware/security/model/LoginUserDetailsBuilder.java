package com.nh.glory.ware.security.model;

import com.nh.glory.data.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

public class LoginUserDetailsBuilder {

    public static LoginUserDetails creatOf(User user, List<String> roles) {
        return new LoginUserDetails(user.getId(), user.getAccount(), user.getPassword(), user.getRoleId(), mapToGrantedAuthorities(roles));
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<String> roles) {
        return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

}
