package com.nh.glory.ware.wrapper;

import com.nh.glory.core.constant.Constants;
import com.nh.glory.core.node.ForestNodeMerger;
import com.nh.glory.core.util.BeanUtil;
import com.nh.glory.core.util.Func;
import com.nh.glory.data.model.Menu;
import com.nh.glory.data.vo.MenuVO;
import com.nh.glory.ware.service.system.DictService;
import com.nh.glory.ware.service.system.MenuService;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.util.Strings;

import java.util.List;
import java.util.stream.Collectors;


@AllArgsConstructor
public class MenuWrapper extends BaseEntityWrapper<Menu, MenuVO> {

	private MenuService menuService;
	private DictService dictService;

	public MenuWrapper() {
	}

	@Override
	public MenuVO entityVO(Menu menu) {
		MenuVO menuVO = BeanUtil.copy(menu, MenuVO.class);
		if (Func.equals(menu.getParentId(), Constants.TOP_PARENT_ID)) {
			menuVO.setParentName(Constants.TOP_PARENT_NAME);
		} else {
			Menu parent = menuService.getById(menu.getParentId());
			menuVO.setParentName(parent.getName());
		}

		String d1 = dictService.getValue("menu_category", Func.toInt(menuVO.getCategory()));
		String d2 = dictService.getValue("button_func", Func.toInt(menuVO.getAction()));
		String d3 = dictService.getValue("yes_no", Func.toInt(menuVO.getOpened()));
		if (Strings.isNotEmpty(d1)) {
			menuVO.setCategoryName(d1);
		}
		if (Strings.isNotEmpty(d2)) {
			menuVO.setActionName(d2);
		}
		if (Strings.isNotEmpty(d3)) {
			menuVO.setOpenedName(d3);
		}
		return menuVO;
	}


	public List<MenuVO> listNodeVO(List<Menu> list) {
		List<MenuVO> collect = list.stream().map(menu -> BeanUtil.copy(menu, MenuVO.class)).collect(Collectors.toList());
		return ForestNodeMerger.merge(collect);
	}

}
