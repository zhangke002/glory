/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nh.glory.ware.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nh.glory.core.constant.Constants;
import com.nh.glory.core.node.ForestNodeMerger;
import com.nh.glory.data.mapper.DeptMapper;
import com.nh.glory.data.model.Dept;
import com.nh.glory.data.vo.DeptVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DeptService extends ServiceImpl<DeptMapper, Dept> {

	public IPage<DeptVO> selectDeptPage(IPage<DeptVO> page, DeptVO dept) {
		return page.setRecords(baseMapper.selectDeptPage(page, dept));
	}

	public List<DeptVO> tree() {
		return ForestNodeMerger.merge(baseMapper.tree());
	}

}
