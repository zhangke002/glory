package com.nh.glory.ware.wrapper;

import com.nh.glory.core.constant.Constants;
import com.nh.glory.core.node.ForestNodeMerger;
import com.nh.glory.core.node.INode;
import com.nh.glory.core.util.BeanUtil;
import com.nh.glory.core.util.Func;
import com.nh.glory.data.model.Dept;
import com.nh.glory.data.vo.DeptVO;
import com.nh.glory.ware.service.system.DeptService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class DeptWrapper extends BaseEntityWrapper<Dept, DeptVO> {

	private DeptService deptService;

	public DeptWrapper() {
	}

	@Override
	public DeptVO entityVO(Dept dept) {
		DeptVO deptVO = BeanUtil.copy(dept, DeptVO.class);
		if (Func.equals(dept.getParentId(), Constants.TOP_PARENT_ID)) {
			deptVO.setParentName(Constants.TOP_PARENT_NAME);
		} else {
			Dept parent = deptService.getById(dept.getParentId());
			deptVO.setParentName(parent.getDeptName());
		}
		return deptVO;
	}


	public List<INode> listNodeVO(List<Dept> list) {
		List<INode> collect = list.stream().map(this::entityVO).collect(Collectors.toList());
		return ForestNodeMerger.merge(collect);
	}

}
