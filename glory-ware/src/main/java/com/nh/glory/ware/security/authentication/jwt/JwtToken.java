package com.nh.glory.ware.security.authentication.jwt;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.jsonwebtoken.Claims;

public final class JwtToken {

    private final String token;

    @JsonIgnore
    private final Claims claims;

    public JwtToken(String token) {
        this.token = token;
        this.claims = null;
    }

    public JwtToken(String token, Claims claims) {
        this.token = token;
        this.claims = claims;
    }

    public String getToken() {
        return this.token;
    }

    public Claims getClaims() {
        return claims;
    }
}
