package com.nh.glory.ware.wrapper;

import com.nh.glory.core.constant.Constants;
import com.nh.glory.core.node.ForestNodeMerger;
import com.nh.glory.core.node.INode;
import com.nh.glory.core.util.BeanUtil;
import com.nh.glory.core.util.Func;
import com.nh.glory.data.model.Dict;
import com.nh.glory.data.vo.DictVO;
import com.nh.glory.ware.service.system.DictService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;


@AllArgsConstructor
public class DictWrapper extends BaseEntityWrapper<Dict, DictVO> {

	private DictService dictService;

	public DictWrapper() {
	}

	@Override
	public DictVO entityVO(Dict dict) {
		DictVO dictVO = BeanUtil.copy(dict, DictVO.class);
		if (Func.equals(dict.getParentId(), Constants.TOP_PARENT_ID)) {
			dictVO.setParentName(Constants.TOP_PARENT_NAME);
		} else {
			Dict parent = dictService.getById(dict.getParentId());
			dictVO.setParentName(parent.getDictValue());
		}
		return dictVO;
	}

	public List<INode> listNodeVO(List<Dict> list) {
//		List<INode> collect = list.stream().map(dict -> BeanUtil.copy(dict, DictVO.class)).collect(Collectors.toList());

		List<INode> collect= list.stream().map(this::entityVO).collect(Collectors.toList());
		return ForestNodeMerger.merge(collect);
	}

}
