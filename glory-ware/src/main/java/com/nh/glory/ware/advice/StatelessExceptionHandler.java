package com.nh.glory.ware.advice;

import com.nh.glory.core.R;
import com.nh.glory.core.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class StatelessExceptionHandler {

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<R> handleNotFoundException(NotFoundException exception) {
        log.warn(exception.getMessage(), exception);
        R bean = R.fail(HttpStatus.NOT_FOUND.value(), exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(bean);
    }

}
