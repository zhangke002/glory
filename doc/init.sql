-- ----------------------------
-- Records of gl_dept
-- ----------------------------
INSERT INTO `gl_dept` VALUES ('1', '0', '呐喊信息', '呐喊信息技术有限公司', '1', '0', '');

-- ----------------------------
-- Records of gl_role
-- ----------------------------
INSERT INTO `gl_role` VALUES ('1', '0', '1', '0', '超级管理员', 'administrator');

-- ----------------------------
-- Records of gl_user
-- ----------------------------
INSERT INTO `gl_user` VALUES ('1', 'admin', '$2a$10$ARugSh8na.cn5/izjAqrlOOPZ2m/LK2TJqvTsTZDli9R9SsWa2rVS', null, 'Admin', 'Admin', null, null, null, '0', '0', '0', '2018-04-08 15:19:15', '1', '1');

-- ----------------------------
-- Records of gl_dict
-- ----------------------------
INSERT INTO `gl_dict` VALUES ('1', '0', 'sex', '-1', '性别', '1', null, '0');
INSERT INTO `gl_dict` VALUES ('2', '1', 'sex', '3', '保密', '1', null, '0');
INSERT INTO `gl_dict` VALUES ('3', '1', 'sex', '1', '男', '2', null, '0');
INSERT INTO `gl_dict` VALUES ('4', '1', 'sex', '2', '女', '3', null, '0');
INSERT INTO `gl_dict` VALUES ('5', '0', 'menu_category', '-1', '菜单类型', '3', null, '0');
INSERT INTO `gl_dict` VALUES ('6', '5', 'menu_category', '1', '菜单', '1', null, '0');
INSERT INTO `gl_dict` VALUES ('7', '5', 'menu_category', '2', '按钮', '2', null, '0');
INSERT INTO `gl_dict` VALUES ('8', '0', 'button_func', '-1', '按钮功能', '4', null, '0');
INSERT INTO `gl_dict` VALUES ('9', '8', 'button_func', '1', '工具栏', '1', null, '0');
INSERT INTO `gl_dict` VALUES ('10', '8', 'button_func', '2', '操作栏', '2', null, '0');
INSERT INTO `gl_dict` VALUES ('11', '8', 'button_func', '3', '工具操作栏', '3', null, '0');
INSERT INTO `gl_dict` VALUES ('12', '0', 'yes_no', '-1', '是否', '5', null, '0');
INSERT INTO `gl_dict` VALUES ('13', '12', 'yes_no', '1', '否', '1', null, '0');
INSERT INTO `gl_dict` VALUES ('14', '12', 'yes_no', '2', '是', '2', null, '0');


-- ----------------------------
-- Records of gl_menu
-- ----------------------------
INSERT INTO `gl_menu` VALUES ('1', '0', 'desk', '工作台', 'menu', '/desk', 'desktop', '1', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('2', '1', 'notice', '通知公告', 'menu', '/desk/notice', null, '1', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('3', '0', 'system', '系统管理', 'menu', '/system', 'setting', '2', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('4', '3', 'user', '用户管理', 'menu', '/system/user', null, '1', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('5', '3', 'dept', '部门管理', 'menu', '/system/dept', null, '2', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('6', '3', 'dict', '字典管理', 'menu', '/system/dict', null, '3', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('7', '3', 'menu', '菜单管理', 'menu', '/system/menu', null, '4', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('8', '3', 'role', '角色管理', 'menu', '/system/role', null, '5', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('9', '3', 'param', '参数管理', 'menu', '/system/param', null, '6', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('10', '0', 'monitor', '系统监控', 'menu', '/monitor', 'fund', '3', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('11', '10', 'doc', '接口文档', 'menu', 'http://localhost/doc.html', null, '1', '1', '0', '2', '0', null);
INSERT INTO `gl_menu` VALUES ('12', '10', 'admin', '服务治理', 'menu', 'http://localhost:7002', null, '2', '1', '0', '2', '0', null);
INSERT INTO `gl_menu` VALUES ('13', '10', 'log', '日志管理', 'menu', '/monitor/log', null, '3', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('14', '13', 'log_usual', '通用日志', 'menu', '/monitor/log/usual', null, '1', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('15', '13', 'log_api', '接口日志', 'menu', '/monitor/log/api', null, '2', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('16', '13', 'log_error', '错误日志', 'menu', '/monitor/log/error', null, '3', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('17', '0', 'tool', '研发工具', 'menu', '/tool', 'tool', '4', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('18', '17', 'code', '代码生成', 'menu', '/tool/code', null, '1', '1', '0', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('19', '2', 'notice_add', '新增', 'add', '/desk/notice/add', 'plus', '1', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('20', '2', 'notice_edit', '修改', 'edit', '/desk/notice/edit', 'form', '2', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('21', '2', 'notice_delete', '删除', 'delete', '/api/blade-desk/notice/remove', 'delete', '3', '2', '3', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('22', '2', 'notice_view', '查看', 'view', '/desk/notice/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('23', '4', 'user_add', '新增', 'add', '/system/user/add', 'plus', '1', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('24', '4', 'user_edit', '修改', 'edit', '/system/user/edit', 'form', '2', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('25', '4', 'user_delete', '删除', 'delete', '/api/blade-user/remove', 'delete', '3', '2', '3', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('26', '4', 'user_role', '角色配置', 'role', null, 'user-add', '4', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('27', '4', 'user_reset', '密码重置', 'reset-password', '/api/blade-user/reset-password', 'retweet', '5', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('28', '4', 'user_view', '查看', 'view', '/system/user/view', 'file-text', '6', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('29', '5', 'dept_add', '新增', 'add', '/system/dept/add', 'plus', '1', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('30', '5', 'dept_edit', '修改', 'edit', '/system/dept/edit', 'form', '2', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('31', '5', 'dept_delete', '删除', 'delete', '/api/blade-system/dept/remove', 'delete', '3', '2', '3', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('32', '5', 'dept_view', '查看', 'view', '/system/dept/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('33', '6', 'dict_add', '新增', 'add', '/system/dict/add', 'plus', '1', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('34', '6', 'dict_edit', '修改', 'edit', '/system/dict/edit', 'form', '2', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('35', '6', 'dict_delete', '删除', 'delete', '/api/blade-system/dict/remove', 'delete', '3', '2', '3', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('36', '6', 'dict_view', '查看', 'view', '/system/dict/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('37', '7', 'menu_add', '新增', 'add', '/system/menu/add', 'plus', '1', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('38', '7', 'menu_edit', '修改', 'edit', '/system/menu/edit', 'form', '2', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('39', '7', 'menu_delete', '删除', 'delete', '/api/blade-system/menu/remove', 'delete', '3', '2', '3', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('40', '7', 'menu_view', '查看', 'view', '/system/menu/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('41', '8', 'role_add', '新增', 'add', '/system/role/add', 'plus', '1', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('42', '8', 'role_edit', '修改', 'edit', '/system/role/edit', 'form', '2', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('43', '8', 'role_delete', '删除', 'delete', '/api/blade-system/role/remove', 'delete', '3', '2', '3', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('44', '8', 'role_view', '查看', 'view', '/system/role/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('45', '9', 'param_add', '新增', 'add', '/system/param/add', 'plus', '1', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('46', '9', 'param_edit', '修改', 'edit', '/system/param/edit', 'form', '2', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('47', '9', 'param_delete', '删除', 'delete', '/api/blade-system/param/remove', 'delete', '3', '2', '3', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('48', '9', 'param_view', '查看', 'view', '/system/param/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('49', '14', 'log_usual_view', '查看', 'view', '/monitor/log/usual/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('50', '15', 'log_api_view', '查看', 'view', '/monitor/log/api/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('51', '16', 'log_error_view', '查看', 'view', '/monitor/log/error/view', 'file-text', '4', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('52', '18', 'code_add', '新增', 'add', '/tool/code/add', 'plus', '1', '2', '1', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('53', '18', 'code_edit', '修改', 'edit', '/tool/code/edit', 'form', '2', '2', '2', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('54', '18', 'code_delete', '删除', 'delete', '/api/blade-system/code/remove', 'delete', '3', '2', '3', '1', '0', null);
INSERT INTO `gl_menu` VALUES ('55', '18', 'code_view', '查看', 'view', '/tool/code/view', 'file-text', '4', '2', '2', '1', '0', null);


-- ----------------------------
-- Records of gl_role_menu
-- ----------------------------
INSERT INTO `gl_role_menu` VALUES ('12', '4', '1');
INSERT INTO `gl_role_menu` VALUES ('13', '23', '1');
INSERT INTO `gl_role_menu` VALUES ('14', '24', '1');
INSERT INTO `gl_role_menu` VALUES ('15', '25', '1');
INSERT INTO `gl_role_menu` VALUES ('16', '26', '1');
INSERT INTO `gl_role_menu` VALUES ('17', '27', '1');
INSERT INTO `gl_role_menu` VALUES ('18', '28', '1');
INSERT INTO `gl_role_menu` VALUES ('19', '5', '1');
INSERT INTO `gl_role_menu` VALUES ('20', '29', '1');
INSERT INTO `gl_role_menu` VALUES ('21', '30', '1');
INSERT INTO `gl_role_menu` VALUES ('22', '31', '1');
INSERT INTO `gl_role_menu` VALUES ('23', '32', '1');
INSERT INTO `gl_role_menu` VALUES ('24', '6', '1');
INSERT INTO `gl_role_menu` VALUES ('25', '33', '1');
INSERT INTO `gl_role_menu` VALUES ('26', '34', '1');
INSERT INTO `gl_role_menu` VALUES ('27', '35', '1');
INSERT INTO `gl_role_menu` VALUES ('28', '36', '1');
INSERT INTO `gl_role_menu` VALUES ('29', '7', '1');
INSERT INTO `gl_role_menu` VALUES ('30', '37', '1');
INSERT INTO `gl_role_menu` VALUES ('31', '38', '1');
INSERT INTO `gl_role_menu` VALUES ('32', '39', '1');
INSERT INTO `gl_role_menu` VALUES ('33', '40', '1');
INSERT INTO `gl_role_menu` VALUES ('34', '8', '1');
INSERT INTO `gl_role_menu` VALUES ('35', '41', '1');
INSERT INTO `gl_role_menu` VALUES ('36', '42', '1');
INSERT INTO `gl_role_menu` VALUES ('37', '43', '1');
INSERT INTO `gl_role_menu` VALUES ('38', '44', '1');

