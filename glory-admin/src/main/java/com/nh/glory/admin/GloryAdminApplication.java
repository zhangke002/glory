package com.nh.glory.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.nh.glory")
public class GloryAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(GloryAdminApplication.class, args);
    }

}
