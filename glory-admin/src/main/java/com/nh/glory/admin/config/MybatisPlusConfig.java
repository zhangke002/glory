package com.nh.glory.admin.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@MapperScan("com.nh.glory.data.mapper")
public class MybatisPlusConfig {

    /**
     * mybatis-plus分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * 乐观锁mybatis插件
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInterceptor();
    }

    /**
     * SQL执行效率插件
     */
//    @Bean
//    @Profile({"dev","test"})
//    public PerformanceInterceptor performanceInterceptor() {
//        return new PerformanceInterceptor();
//    }

//    @ConfigurationProperties(prefix = "mybatis-plus-query")
//    @Bean
//    public QueryConfigProperty queryConfigProperty() {
//        return new QueryConfigProperty();
//    }
//
//    @Bean
//    public QuerySupportMethod querySupportMethod() {
//        return new QuerySupportMethod();
//    }
//
//    @Bean
//    public QuerySupportSqlInjector querySupportSqlInjector() {
//        return new QuerySupportSqlInjector();
//    }
}
