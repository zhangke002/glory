package com.nh.glory.admin.modular.system.controller;

import com.nh.glory.core.R;
import com.nh.glory.core.util.Func;
import com.nh.glory.data.model.Menu;
import com.nh.glory.data.vo.MenuVO;
import com.nh.glory.ware.security.model.LoginUserDetails;
import com.nh.glory.ware.service.system.DictService;
import com.nh.glory.ware.service.system.MenuService;
import com.nh.glory.ware.support.Condition;
import com.nh.glory.ware.support.Kv;
import com.nh.glory.ware.wrapper.MenuWrapper;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 控制器
 *
 * @author Chill
 */
@RestController
@AllArgsConstructor
@RequestMapping("/menu")
public class MenuController {

	private MenuService menuService;

	private DictService dictService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	public R<MenuVO> detail(@RequestParam Long id) {
		Menu detail = menuService.getById(id);
		MenuWrapper menuWrapper = new MenuWrapper(menuService, dictService);
		return R.ok(menuWrapper.entityVO(detail));
	}

	/**
	 * 列表
	 */
	@GetMapping("/list")
	public R<List<MenuVO>> list(@RequestParam Map<String, Object> menu) {
		List<Menu> list = menuService.list(Condition.getQueryWrapper(menu, Menu.class).lambda().orderByAsc(Menu::getSort));
		MenuWrapper menuWrapper = new MenuWrapper(menuService, dictService);
		return R.ok(menuWrapper.listNodeVO(list));
	}

	/**
	 * 前端菜单数据
	 */
	@GetMapping("/routes")
	public R<List<MenuVO>> routes() {
		LoginUserDetails principal = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<MenuVO> list = menuService.routes(principal.getRoleId().toString());
		return R.ok(list);
	}

	/**
	 * 前端按钮数据
	 */
	@GetMapping("/buttons")
	public R<List<MenuVO>> buttons() {
		LoginUserDetails principal = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<MenuVO> list = menuService.buttons(principal.getRoleId().toString());
		return R.ok(list);
	}

	/**
	 * 获取菜单树形结构
	 */
	@GetMapping("/tree")
	public R<List<MenuVO>> tree() {
		List<MenuVO> tree = menuService.tree();
		return R.ok(tree);
	}

	/**
	 * 获取权限分配树形结构
	 */
	@GetMapping("/grant-tree")
	public R<List<MenuVO>> grantTree() {
		return R.ok(menuService.grantTree());
	}

	/**
	 * 获取权限分配树形结构
	 */
	@GetMapping("/role-tree-keys")
	public R<List<String>> roleTreeKeys(String roleIds) {
		return R.ok(menuService.roleTreeKeys(roleIds));
	}

	/**
	 * 新增或修改
	 */
	@PostMapping("/submit")
	public R submit(@Valid @RequestBody Menu menu) {
		if (menu.getId() == null) {
			if (menu.getParentId() == null) {
				menu.setParentId(0L);
			}

			if (menu.getDeleted() == null){
				menu.setDeleted(0);
			}

		}
		return R.status(menuService.saveOrUpdate(menu));
	}


	/**
	 * 删除
	 */
	@DeleteMapping("/remove")
	public R remove(@RequestParam String ids) {
		return R.status(menuService.removeByIds(Func.toIntList(ids)));
	}

	/**
	 * 获取配置的角色权限
	 */
	@GetMapping("auth-routes")
	public R<List<Kv>> authRoutes() {
		LoginUserDetails principal = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return R.ok(menuService.authRoutes(principal.getRoleId().toString()));
	}

}
