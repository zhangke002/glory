/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nh.glory.data.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("gl_menu")
public class Menu implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 菜单父主键
	 */
	private Long parentId;

	/**
	 * 菜单编号
	 */
	private String code;

	/**
	 * 菜单名称
	 */
	private String name;

	/**
	 * 菜单别名
	 */
	private String alias;

	/**
	 * 请求地址
	 */
	private String path;

	/**
	 * 菜单资源
	 */
	private String source;

	/**
	 * 排序
	 */
	private Integer sort;

	/**
	 * 菜单类型: 1 菜单, 2 按钮
	 */
	private Integer category;

	/**
	 * 操作按钮类型: 0 工具栏, 1 操作栏, 2 工具操作栏
	 */
	private Integer action;

	/**
	 * 是否打开新页面: 0 否, 1 是
	 */
	private Integer opened;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 是否已删除
	 */
	@TableLogic
	private Integer deleted;

}
