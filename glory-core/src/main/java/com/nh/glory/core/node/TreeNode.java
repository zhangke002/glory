package com.nh.glory.core.node;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 树型节点
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TreeNode extends BaseNode {

	private String title;

	private Integer key;

	private Integer value;

}
