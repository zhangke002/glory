package com.nh.glory.core.exception;

public class NotFoundException extends RuntimeException {

    private static final String BASE_MSG = "entity not found, hints: [ %s ]";


    public NotFoundException(String value) {
        super(String.format(BASE_MSG, value));
    }

    public NotFoundException(Throwable e) {
        super(e);
    }

    public NotFoundException(String message, Throwable e) {
        super(message, e);
    }

}
