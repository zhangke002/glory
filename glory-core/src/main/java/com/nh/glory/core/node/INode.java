package com.nh.glory.core.node;

import java.util.List;

public interface INode {

    Long getId();

    Long getParentId();

    List<INode> getChildren();
}
