package com.nh.glory.core;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private int code;
    private String msg;
    private T data;

    private R() {
        this(200, null, null);
    }

    private R(int code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public static <T> R<T> ok() {
        return new R<>(200, null, "操作成功");
    }

    public static <T> R<T> ok(T data) {
        return new R<>(200, data, null);
    }

    public static <T> R<T> fail(int code, String msg) {
        return new R<>(code, null, msg);
    }

    public static <T> R<T> status(boolean flag) {
        return flag ? ok() : fail(400,"操作失败");
    }
}
