package com.nh.glory.core.constant;

public final class Constants {

    public static final Integer DEFAULT_PAGE_SIZE = 20;

    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static final String AUTHORIZATION_BEARER_PREFIX = "Bearer ";

    public static final String JWT_CLAIM_USERID = "uid";

    public static final String SEPARATOR = ",";

    public static final String TOKEN_KEY = "token";

    /**
     * 顶级父节点id
     */
    public static final Long TOP_PARENT_ID = 0L;

    /**
     * 顶级父节点名称
     */
    public static final String TOP_PARENT_NAME = "顶级";


    /**
     * 默认密码
     */
    public static final String DEFAULT_PASSWORD = "123456";
}
